# author: hadrihl <hadrihilmi@gmail.com>
# 
# description: a simple traCI interface for 2veh-traci

# Check SUMO path
import os, sys

if 'SUMO_HOME' in os.environ:
	tools = os.path.join(os.environ['SUMO_HOME'], 'tools')
	sys.path.append(tools)
	print("SUMO_HOME path found!: " + tools)
else: 
	sys.exit("Please declare environment variable 'SUMO_HOME'")


sumoCmd = ["sumo-gui", "-c", "square.sumo.cfg"]
import traci
traci.start(sumoCmd)

step = 0
while step < 1000:
	traci.simulationStep()
	print("step: ", step)
	step += 1

traci.close()